<?php

/**
 * Yzncms
 * 版权所有 Yzncms，并保留所有权利。
 * Author: 御宅男 <530765310@qq.com>
 * Update: TopAdmin <8355763@qq.com>
 * Date: 2021/11/16
 * CMS设置
 */

namespace app\admin\controller\cms;

use app\common\controller\Adminbase;
use think\facade\Cookie;

class Setting extends Adminbase
{

    public function change(){
        $siteIds   = $this->auth->sites;
        $site = $this->request->request("type");
        if($site){
            Cookie::set('manageSiteId',$site);
            Cookie::set('publishMode',1);
            $this->success("切换成功！");
        } else {
            Cookie::set('manageSiteId',$site);
            Cookie::set('publishMode',0);
            $this->success("切换成功！");
        }
    }

}
