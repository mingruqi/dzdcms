<?php
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 御宅男 <530765310@qq.com>
// +----------------------------------------------------------------------
// | 栏目模型
// +----------------------------------------------------------------------

namespace app\admin\model\cms;

use \think\Model;

class LangData extends Model
{
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
}
